package com.aurbano;

/**
 * @author Adri�n Urbano
 */
public interface Builder<T> {
     T build();
}
