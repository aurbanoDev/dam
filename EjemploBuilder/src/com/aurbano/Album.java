package com.aurbano;

/**
 * Clase que representa de manera inmutable Un album de m�sica.
 * @author Adri�n Urbano
 */
public class Album {

    private final String nombre;
    private final String grupo;
    private final int numeroCanciones;
    private final int copiasVendidas;
    private final float duracionMinutos;

    public static class Builder implements com.aurbano.Builder<Album> {
        //Atributo obligatorio
        private final String nombre;

        //Atributos opcionales inicilizados por defecto
        private String grupo = "";
        private int copiasVendidas = 0;
        private int numeroCanciones = 0;
        private float duracionMinutos = 0;

        public Builder(String nombre) {
            this.nombre = nombre;
        }

        public Builder grupo(String grupo) {
            this.grupo = grupo;
            return this;
        }

        public Builder numeroCanciones(int numeroCanciones) {
            this.numeroCanciones = numeroCanciones;
            return this;
        }

        public Builder copiasVendidas(int copiasVendidas) {
            this.copiasVendidas = copiasVendidas;
            return this;
        }

        public Builder duracionMinutos(float duracionMinutos) {
            this.duracionMinutos = duracionMinutos;
            return this;
        }

        @Override
        public Album build() {
            return new Album(this);
        }
    }

    public Album(Builder builder) {
        this.nombre = builder.nombre;
        this.grupo = builder.grupo;
        this.numeroCanciones = builder.numeroCanciones;
        this.copiasVendidas = builder.copiasVendidas;
        this.duracionMinutos = builder.duracionMinutos;
    }
}
