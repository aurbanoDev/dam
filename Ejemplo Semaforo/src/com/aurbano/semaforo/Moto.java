package com.aurbano.semaforo;

import java.util.concurrent.Semaphore;

public class Moto implements Runnable {

    private String modelo;
    private Semaphore semaforo;

    public Moto(String modelo, Semaphore semaforo) {
        this.modelo = modelo;
        this.semaforo = semaforo;
    }

    @Override
    public void run() {

        try {
            semaforo.acquire();
            System.out.println("Puede circular la moto del modelo : " + modelo);
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            semaforo.release();
        }

    }
}
