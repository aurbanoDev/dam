package com.aurbano.semaforo;

import java.util.concurrent.Semaphore;

/**
 * Test para limitar a 2 el numero de hilos activos simultaneamente usando un sem�foro.
 */
public class Main {

    public static void main(String[] args) {
        Semaphore semaforo = new Semaphore(2, true);

        String[] modelos = {"Harley", "Kawasaki", "Honda", "Suzuki", "Triumph", "Ducati"};
        Moto[] listaMotos = new Moto[modelos.length];

        for (int i = 0; i < listaMotos.length; i++) {
            listaMotos[i] = new Moto(modelos[i], semaforo);
            new Thread(listaMotos[i]).start();
        }

    }
}
